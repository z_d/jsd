#!/bin/bash
#Update
sudo yum -y update
sudo yum -y install epel-release
sudo yum -y update

#Install JAVA 11
sudo yum install -y java-11-openjdk-devel

#Install Python 3.6
sudo yum -y install python3
sudo yum -y groupinstall 'development tools'

#Install modules Python 3 
sudo pip3 install flask_babelex
sudo pip3 install ldap3
sudo pip3 install alembic

#Install other soft
sudo yum -y install mc nano wget

#Install PostgreSql11
sudo rpm -Uvh https://yum.postgresql.org/11/redhat/rhel-7-x86_64/pgdg-redhat-repo-latest.noarch.rpm
sudo yum install -y postgresql11-server postgresql11
/usr/pgsql-11/bin/postgresql-11-setup initdb
systemctl enable postgresql-11
systemctl start postgresql-11

#Create database in Postgre SQL 11
echo -e '                     \033[41mCreating password to user postgres\033[0m'
sudo -u postgres psql -c "alter user postgres with ENCRYPTED password 'postgres'"
echo -e '                          \033[41mCreating database\033[0m'
sudo -u postgres psql -c "CREATE DATABASE jsd_db"
echo -e '                     \033[41mCreating user jira with password  \033[0m'
sudo -u postgres psql -c "CREATE USER jira with ENCRYPTED PASSWORD '123'"
echo -e '                     \033[41mAssigning privileges to a database\033[0m'
sudo -u postgres psql -c "grant all privileges on database jsd_db to jira"
echo
echo
echo -e '                     \033[41mCreate pg_hda.conf\033[0m'

/bin/mv /var/lib/pgsql/11/data/pg_hba.conf /var/lib/pgsql/11/data/pg_hba.conf.bkp
{
echo # TYPE  DATABASE        USER            ADDRESS                 METHOD
echo
echo # "local" is for Unix domain socket connections only
echo local   all             all                                     peer
echo # IPv4 local connections:
echo host    all             all             127.0.0.1/32            password
echo # IPv6 local connections:
echo host    all             all             ::1/128                 password
echo # Allow replication connections from localhost, by a user with the
echo # replication privilege.
echo local   replication     all                                     peer
echo host    replication     all             127.0.0.1/32            password
echo host    replication     all             ::1/128                 password
echo #
echo host	all		all		0.0.0.0/0		md5
} > /var/lib/pgsql/11/data/pg_hba.conf

#Install Apache Web-Server
yum -y update httpd
yum -y install httpd
systemctl enable httpd
systemctl restart httpd

#Install pgAdmin 4
sudo yum install pgadmin4-web -y
sudo cp /etc/httpd/conf.d/pgadmin4.conf.sample /etc/httpd/conf.d/pgadmin4.conf
sudo mkdir -p /var/lib/pgadmin4/ /var/log/pgadmin4/
sudo chown -R apache:apache /var/lib/pgadmin4 /var/log/pgadmin4
sudo  /usr/pgadmin4/bin/pgadmin4-web-setup.sh

#Firewall rules add
firewall-cmd --add-service=postgresql --permanent
firewall-cmd --add-service=http --permanent
firewall-cmd --zone=public --add-port=8080/tcp --permanent
firewall-cmd --reload

#Install Jira Service Desk
wget https://www.atlassian.com/software/jira/downloads/binary/atlassian-servicedesk-4.13.1-x64.bin
bash atlassian*
echo -e '                                      DONE!'
#reboot

