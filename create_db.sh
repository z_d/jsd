#!/bin/bash
#Create database in Postgre SQL 11

echo -e '                     \033[41mCreating password to user postgres\033[0m'
sudo -u postgres psql -c "alter user postgres with password 'postgres'"
echo -e '                          \033[41mCreating database\033[0m'
sudo -u postgres psql -c "CREATE DATABASE jsd_db"
echo -e '                     \033[41mCreating user jira with password  \033[0m'
sudo -u postgres psql -c "CREATE USER jira with ENCRYPTED PASSWORD '123'"
echo -e '                     \033[41mAssigning privileges to a database\033[0m'
sudo -u postgres psql -c "grant all privileges on database jsd_db to jira"
echo
echo
echo -e '                     \033[41mCreate pg_hda.conf\033[0m'

/bin/mv /var/lib/pgsql/11/data/pg_hba.conf /var/lib/pgsql/11/data/pg_hba.conf.bkp
{
echo # TYPE  DATABASE        USER            ADDRESS                 METHOD
echo
echo # "local" is for Unix domain socket connections only
echo local   all             all                                     peer
echo # IPv4 local connections:
echo host    all             all             127.0.0.1/32            password
echo # IPv6 local connections:
echo host    all             all             ::1/128                 password
echo # Allow replication connections from localhost, by a user with the
echo # replication privilege.
echo local   replication     all                                     peer
echo host    replication     all             127.0.0.1/32            password
echo host    replication     all             ::1/128                 password
echo #
echo host	all		all		0.0.0.0/0		md5
} > /var/lib/pgsql/11/data/pg_hba.conf
exit
